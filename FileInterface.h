//
// Created by bry on 11/11/17.
//

#ifndef GROUPPROJECT_FILEINTERFACE_H
#define GROUPPROJECT_FILEINTERFACE_H


#include <cstdio>
#include "Record.h"

class FileInterface {

private:

    FILE *file;

public:

    FileInterface();

    explicit FileInterface (const char*);

    ~FileInterface();

    bool Read(Record&) const;

};


#endif //GROUPPROJECT_FILEINTERFACE_H
